﻿using UnityEngine;
using System.Collections;
public class InputHandler : MonoBehaviour
{
    private Tile firstSelect;
    [SerializeField]
    private BackButtonIngame backbuttonscript;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            backbuttonscript.Pressed();
        if (Globals.OrbInPlay)
        {
            if (firstSelect)
            {
                firstSelect.SetMatDefault();
                firstSelect = null;
            }
            return;
        }
        bool framelocked = false;
        for (int i = 0; i < Input.touchCount; ++i)
        {
            if (framelocked)
                break;
            if (Input.GetTouch(i).phase == TouchPhase.Ended)
            {
                Vector3 pos = Input.GetTouch(i).position;
                RaycastHit hit;
                if (Physics.Raycast(Camera.main.ScreenPointToRay(pos), out hit))
                {
                    Tile tmp = hit.transform.GetComponent<Tile>();
                    if (tmp && !tmp.locked)
                    {
                        framelocked = true;
                        if (firstSelect)
                        {
                            Vector3 firstPos = firstSelect.transform.position;
                            firstSelect.transform.position = tmp.transform.position;
                            tmp.transform.position = firstPos;
                            firstSelect.SetMatDefault();
                            firstSelect = null;
                        }
                        else
                        {
                            firstSelect = tmp;
                            firstSelect.SetMatSelected();
                        }
                    }
                }
                else
                {
                    firstSelect.SetMatDefault();
                    firstSelect = null;
                }
            }
        }
        if (Application.isEditor)
        {
            if (Input.GetMouseButtonDown(0))
            {
                Vector3 pos = Input.mousePosition;
                RaycastHit hit;
                if (Physics.Raycast(Camera.main.ScreenPointToRay(pos), out hit))
                {
                    Tile tmp = hit.transform.GetComponent<Tile>();
                    if (tmp && !tmp.locked)
                    {
                        if (firstSelect)
                        {
                            StartCoroutine(VisualSwap(firstSelect.transform, tmp.transform));
                            //Vector3 firstPos = firstSelect.transform.position;
                            //firstSelect.transform.position = tmp.transform.position;
                            //tmp.transform.position = firstPos;
                            firstSelect.SetMatDefault();
                            firstSelect = null;
                        }
                        else
                        {
                            firstSelect = tmp;
                            firstSelect.SetMatSelected();
                        }
                    }
                }
                else
                {
                    if (firstSelect)
                    {
                        firstSelect.SetMatDefault();
                        firstSelect = null;
                    }
                }
            }
        }
    }
    private IEnumerator VisualSwap(Transform first, Transform second)
    {
        Vector3 targetPosFirst = second.position;
        Vector3 targetPosSecond = first.position;
        float timer = 0.1f, currentTime = 0.0f;
        while (currentTime < timer)
        {
            currentTime += Time.deltaTime;
            first.position = Vector3.Lerp(first.position, targetPosSecond + new Vector3(0,1,0), Mathf.Clamp01(currentTime / timer));
            yield return null;
        }
        timer = 0.5f;
        currentTime = 0.0f;
        while (currentTime < timer)
        {
            currentTime += Time.deltaTime;
            float pct = Mathf.Clamp01(currentTime / timer);
            first.position = Vector3.Lerp(first.position, targetPosFirst, pct);
            second.position = Vector3.Lerp(second.position, targetPosSecond, pct);
            yield return null;
        }
        first.position = targetPosFirst;
        second.position = targetPosSecond;
    }
}
