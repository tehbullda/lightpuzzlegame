﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class LevelPack : MonoBehaviour {

    [NonSerialized]
    public List<GameObject> levels = new List<GameObject>();
    public GameObject levelObjPrefab;

    public Level LoadLevel(int overrideLevel = -1)
    {
        DeactivateChildren();
        levels[overrideLevel != -1 ? overrideLevel : Globals.currentLevel].SetActive(true);
        return levels[overrideLevel != -1 ? overrideLevel : Globals.currentLevel].GetComponent<Level>();
    }
    public void ReloadLevels()
    {
        levels.Clear();
        for (int i = 0; i < transform.childCount; ++i)
        {
            var g = transform.GetChild(i).gameObject;
            g.SetActive(false);
            levels.Add(g);
        }
    }
    public void NewLevel()
    {
        ReloadLevels();
        GameObject tmp = Instantiate(levelObjPrefab, transform.position, Quaternion.identity, transform);
        tmp.name = "Level " + transform.childCount;
        levels.Add(tmp);
    }
    public void DeactivateChildren()
    {
        ReloadLevels();
        for (int i = 0; i < levels.Count; ++i)
        {
            levels[i].SetActive(false);
        }
    }
}
#if UNITY_EDITOR
[CustomEditor(typeof(LevelPack))]
public class Loader : Editor
{
    public int editorLevelLoaded = -1;
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        LevelPack script = (LevelPack)target;
        string[] strings = new string[script.levels.Count];
        for (int i = 0; i < strings.Length; ++i)
        {
            strings[i] = (i + 1).ToString();
        }
        editorLevelLoaded = EditorGUILayout.Popup(editorLevelLoaded, strings);
        if (GUILayout.Button("Load Level"))
        {
            script.LoadLevel(editorLevelLoaded);
        }
        if (GUILayout.Button("New Level"))
        {
            script.NewLevel();
        }
    }
}
#endif
