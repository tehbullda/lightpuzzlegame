﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class BackButtonIngame : MonoBehaviour
{
    private bool pressedOnce = false;
    [SerializeField]
    private float secondPressTimer = 3.0f;
    public void Pressed()
    {
        if (!pressedOnce)
            StartCoroutine(DisplaySecondPressText());
        else
            SceneManager.LoadScene(0);
    }
    private IEnumerator DisplaySecondPressText()
    {
        Text text = GetComponentInChildren<Text>(true);
        text.gameObject.SetActive(true);
        pressedOnce = true;
        Color startCol = text.color;
        Color endCol = startCol;
        endCol.a = 0.0f;
        float currentTime = 0.0f;
        while (currentTime < secondPressTimer)
        {
            currentTime += Time.deltaTime;
            float pct = Mathf.Clamp01(currentTime / secondPressTimer);
            text.color = Color.Lerp(startCol, endCol, pct);
            yield return new WaitForEndOfFrame();
        }
        pressedOnce = false;
        text.color = startCol;
        text.gameObject.SetActive(false);
        yield return null;
    }
}
