﻿using UnityEngine;
using System.Collections;

public class ClearSaveButton : MonoBehaviour {

	public void Clicked()
    {
        PlayerPrefs.DeleteAll();
    }
}
