﻿using UnityEngine;
using System.Collections;

public class NextLevelButton : MonoBehaviour
{
    [SerializeField]
    private GridHandler gridHandler;
    void Start()
    {
        SetActiveFromResult();
    }
    public void Activate()
    {
        gameObject.SetActive(true);
    }
    public void Clicked()
    {
        gridHandler.ResetOrb();
        gridHandler.LoadNextLevel();
        SetActiveFromResult();
    }
    public void SetActiveFromResult()
    {
        bool result = false;
        if (PlayerPrefs.HasKey(Globals.levelSaveBaseStr + Globals.currentLevel))
        {
            if (PlayerPrefs.GetInt(Globals.levelSaveBaseStr + Globals.currentLevel) == 1)
            {
                Debug.Log("Level " + Globals.currentLevel + " is completed.");
                result = true;
            }
        }
        gameObject.SetActive(result);
    }
}
