﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class WDictionary<TKey, TValue> : Dictionary<TKey, TValue>, ISerializationCallbackReceiver
{
    public List<TKey> keys = new List<TKey>();
    public List<TValue> values = new List<TValue>();
    // save the dictionary to lists
    public void OnBeforeSerialize()
    {
        //keys.Clear();
        //values.Clear();
        //foreach (KeyValuePair<TKey, TValue> pair in this)
        //{
        //    keys.Add(pair.Key);
        //    values.Add(pair.Value);
        //}
        while (keys.Count > values.Count)
        {
            values.Add(default);
        }
        while (keys.Count < values.Count)
        {
            values.RemoveAt(values.Count - 1);
        }
    }

    // load dictionary from lists
    public void OnAfterDeserialize()
    {
        this.Clear();

        if (keys.Count != values.Count)
        {
            Debug.LogError(string.Format("there are {0} keys and {1} values after deserialization. Make sure that both key and value types are serializable.", keys.Count, values.Count));
            return;
        }

        for (int i = 0; i < keys.Count; i++)
            this.Add(keys[i], values[i]);
    }
}
[System.Serializable]
public class TileSetupDictionary : WDictionary<GridHandler.TileType, GameObject>{ }

[CreateAssetMenu(fileName ="TileInfo", menuName ="Tiles/Information", order = 1)]
public class TileInformationResource : ScriptableObject {

    public TileSetupDictionary tilePrefabs;
}
