﻿using UnityEngine;
using System.Collections;

public class SimpleSingletonBehaviour<T> : SingletonBehaviour<T> where T : SingletonBehaviour<T>
{
    static public new T Instance
    {
        get
        {
            if (_instance == null)
            {
                Debug.LogError("Cannot find singleton of type " + typeof(T).GetType().ToString());
            }
            else
            {
                // Debug.Log("<Color=cyan>_Returning singleton </color> <color=white> " + typeof(T).FullName + "</color>");
                if (!_instance.gameObject.activeInHierarchy)
                {
                    _instance.gameObject.SetActive(true);
                }
            }
            return _instance;
        }
    }

}
