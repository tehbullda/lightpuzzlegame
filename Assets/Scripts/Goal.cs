﻿using UnityEngine;
using System.Collections;

public class Goal : MonoBehaviour {
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "MainOrb")
        {
            StartCoroutine(other.GetComponent<OrbMovement>().Kill(transform.position + Globals.orbSpawnOffset, false));
            StartCoroutine(PlayCompletionFeedback());
            transform.parent.GetComponent<Level>().Save();
            GameObject.Find("Canvas").transform.GetChild(2).GetComponent<NextLevelButton>().SetActiveFromResult();
        }
    }
    public IEnumerator PlayCompletionFeedback()
    {
        GetComponentInChildren<ParticleSystem>().Play();
        float timer = 2.0f, currenttime = 0.0f;
        while (currenttime < timer)
        {
            currenttime += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        GetComponentInChildren<ParticleSystem>().Stop();
        yield return null;
    }
}
