﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class LevelSelectHandler : MonoBehaviour
{

    private LevelTile[] levels;
    private Color defaultColor;
    [SerializeField]
    private Color selectedColor;
    void Start()
    {
        levels = GetComponentsInChildren<LevelTile>();
        int additionalunlocks = 0;
        for (int i = 0; i < transform.childCount; ++i)
        {
            levels[i].locked = true;
            if (levels[i].completed)
            {
                additionalunlocks++;
            }
        }
        for (int i = 0; i < levels.Length; ++i)
        {
            if (i < 3 + additionalunlocks)
                levels[i].locked = false;
            levels[i].UpdateSprite();
        }
        Globals.currentLevel = 0;
        defaultColor = transform.GetChild(0).GetComponent<Image>().color;
        transform.GetChild(Globals.currentLevel).GetComponent<Image>().color = selectedColor;
    }
    public void SetLevelSelected(LevelTile tileSelected)
    {
        int index = tileSelected.index;
        if (index == Globals.currentLevel || levels[index].locked)
            return;
        transform.GetChild(Globals.currentLevel).GetComponent<Image>().color = defaultColor;
        Globals.currentLevel = index;
        transform.GetChild(index).GetComponent<Image>().color = selectedColor;
    }

}
