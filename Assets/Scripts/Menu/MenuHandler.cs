﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using System.Runtime.CompilerServices;

public class MenuHandler : MonoBehaviour 
{
    public enum MenuDestination
    {
        Main,
        LevelSelect,
        Settings,
        Shop,
        Count
    }
    [SerializeField, Tooltip("Main, Level, Settings, Shop")]
    private GameObject[] menuObjects;
    private bool firstExitPressed = false;
    [SerializeField]
    private float timerForSecondPress = 2.0f;
    [SerializeField]
    private Text quitConfirmText;
    void Awake()
    {
        Application.targetFrameRate = 60;
        Input.backButtonLeavesApp = false;
    }
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.Escape))
        {
            if (menuObjects[(int)MenuDestination.Main].activeSelf)
            {
                if (!firstExitPressed)
                    StartCoroutine(DisplaySecondPressText());
                else
                    Application.Quit();
            }
            else
            {
                GotoMenu(MenuDestination.Main);
            }
        }
    }
    private IEnumerator DisplaySecondPressText()
    {
        quitConfirmText.gameObject.SetActive(true);
        firstExitPressed = true;
        Color startCol = quitConfirmText.color;
        Color endCol = startCol;
        endCol.a = 0.0f;
        float currentTime = 0.0f;
        while (currentTime < timerForSecondPress)
        {
            currentTime += Time.deltaTime;
            float pct = Mathf.Clamp01(currentTime / timerForSecondPress);
            quitConfirmText.color = Color.Lerp(startCol, endCol, pct);
            yield return null;
        }
        firstExitPressed = false;
        quitConfirmText.color = startCol;
        quitConfirmText.gameObject.SetActive(false);
    }
    void OnEnable()
    {
        GotoMenu(MenuDestination.Main);
    }
    public void GotoMenu(MenuDestination destination)
    {
        DeactivateAll();
        menuObjects[(int)destination].SetActive(true);
    }
    public void GotoMenu(string menu)
    {
        switch (menu)
        {
            case "Main":
                GotoMenu(MenuDestination.Main);
                break;
            case "Level":
                GotoMenu(MenuDestination.LevelSelect);
                break;
            case "Settings":
                GotoMenu(MenuDestination.Settings);
                break;
            case "Shop":
                GotoMenu(MenuDestination.Shop);
                break;
        }
    }
    private void DeactivateAll()
    {
        for (int i = 0; i < menuObjects.Length; ++i)
        {
            menuObjects[i].SetActive(false);
        }
    }
}
