﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.IO;

public class LevelTile : MonoBehaviour
{
    public GameObject lockedObj, completedObj;
    private Image img;
    [HideInInspector]
    public bool locked, completed;
    [HideInInspector]
    public int index;
    private Text text;
    void Awake()
    {
        text = GetComponentInChildren<Text>();
        img = GetComponent<Image>();
        string[] texts = transform.name.Split(' ');
        index = System.Convert.ToInt32(texts[1]);
        text.text = index.ToString();
        --index;
        if (PlayerPrefs.HasKey(Globals.levelSaveBaseStr + texts[1]))
        {
            int progress = PlayerPrefs.GetInt(Globals.levelSaveBaseStr + texts[1]);
            completed = progress == 1 ? true : false;
        }
        else
        {
            completed = false;
        }
        UpdateSprite();
    }
    public void UpdateSprite()
    {
        GetComponent<Button>().interactable = true;
        if (lockedObj.activeSelf)
            lockedObj.SetActive(false);
        if (completedObj.activeSelf)
            completedObj.SetActive(false);
        if (text.text == "")
        {
            text.text = index.ToString();
        }
        if (completed)
        {
            completedObj.SetActive(true);
        }
        else if (locked)
        {
            GetComponentInChildren<Text>().text = "";
            GetComponent<Button>().interactable = false;
            lockedObj.SetActive(true);
        }
    }
}
