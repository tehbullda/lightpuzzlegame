﻿using UnityEngine;
using System.Collections;

public class OrbMovement : MonoBehaviour
{
    public enum MoveDir
    {
        Up,
        Down,
        Left,
        Right,
        None
    }
    [HideInInspector]
    public Vector3 currentDir, nextDir, currentGoal;
    public float moveSpeed, closeEnoughDistance;
    private bool hasGoal = false, stopped = false;
    [HideInInspector]
    public MoveDir currentMoveDir;
    [HideInInspector]
    public Orbspawner spawner;
    [SerializeField]
    private Renderer[] orbFeedbackRenderers;
    [SerializeField]
    private Light orbLight;
    void Update()
    {
        if (stopped)
        {
            if (Vector3.Distance(transform.position, currentGoal) > closeEnoughDistance)
            {
                transform.position += currentDir * moveSpeed * Time.deltaTime;
            }
            return;
        }
        transform.position += currentDir * moveSpeed * Time.deltaTime;
        if (hasGoal)
        {
            if (Vector3.Distance(transform.position, currentGoal) < closeEnoughDistance)
            {
                transform.position = currentGoal;
                currentDir = nextDir;
                UpdateMoveDir();
                hasGoal = false;
            }
        }
    }
    public IEnumerator Kill(Vector3 goalPos, bool died = true)
    {
        stopped = true;
        currentGoal = goalPos;
        TrailRenderer rend = new TrailRenderer();
        if (died)
            GetComponentInChildren<ParticleSystem>().Play();
        else
        {
            rend = (TrailRenderer)orbFeedbackRenderers[orbFeedbackRenderers.Length - 1];
            rend.time = 2.5f;
        }
        float timer = 1.0f, currenttime = 0.0f;
        float startIntensity = orbLight.intensity;
        while (currenttime < timer)
        {
            float pct = Mathf.Clamp01(currenttime / timer);
            if (died)
            {
                orbLight.intensity = startIntensity * (1.0f - pct);
                for (int i = 0; i < orbFeedbackRenderers.Length; ++i)
                {
                    orbFeedbackRenderers[i].material.SetFloat("_Opacity", 1.0f - pct);
                }
            }
            else
            {
                rend.endWidth = 0.5f * (1.0f - pct);
            }
            currenttime += Time.deltaTime;
            yield return null;
        }
        for (int i = 0; i < orbFeedbackRenderers.Length; ++i)
        {
            orbFeedbackRenderers[i].material.SetFloat("_Opacity", 1.0f);
        }
        if (died)
            spawner.ToggleOrb(false);
    }
    public void SetNextDirection(Vector3 nextDirection, bool hasGoalBeforeTurn = false, Vector3 goalBeforeTurn = default(Vector3))
    {
        if (hasGoalBeforeTurn)
        {
            hasGoal = true;
            nextDir = nextDirection;
            currentGoal = goalBeforeTurn;
        }
        else
        {
            currentDir = nextDirection;
            UpdateMoveDir();
        }
    }
    public void SetNextDirection(MoveDir nextDirection, bool hasGoalBeforeTurn = false, Vector3 goalBeforeTurn = default(Vector3))
    {
        SetNextDirection(DirToVector3(nextDirection), hasGoalBeforeTurn, goalBeforeTurn);
    }
    public MoveDir GetCurrentDir()
    {
        return currentMoveDir;
    }
    public Vector3 DirToVector3(MoveDir dir)
    {
        switch (dir)
        {
            case MoveDir.Up:
                return Vector3.left;
            case MoveDir.Down:
                return -Vector3.left;
            case MoveDir.Left:
                return -Vector3.forward;
            case MoveDir.Right:
                return Vector3.forward;
            default:
                return default;
        }
    }
    public static MoveDir InvertedDir(MoveDir inDir)
    {
        switch (inDir)
        {
            case MoveDir.Up:
                return MoveDir.Down;
            case MoveDir.Down:
                return MoveDir.Up;
            case MoveDir.Left:
                return MoveDir.Right;
            case MoveDir.Right:
                return MoveDir.Left;
            default:
                return MoveDir.None;
        }
    }
    public void Stop(Vector3 stoppingPos)
    {
        hasGoal = true;
        currentGoal = stoppingPos;
        stopped = true;
    }
    private void UpdateMoveDir()
    {
        if (currentDir == Vector3.forward)
        {
            currentMoveDir = MoveDir.Right;
        }
        else if (currentDir == -Vector3.forward)
        {
            currentMoveDir = MoveDir.Left;
        }
        else if (currentDir == Vector3.left)
        {
            currentMoveDir = MoveDir.Up;
        }
        else if (currentDir == -Vector3.left)
        {
            currentMoveDir = MoveDir.Down;
        }
    }
}
