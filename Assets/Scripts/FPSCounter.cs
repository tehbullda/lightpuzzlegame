﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class FPSCounter : MonoBehaviour {

    private float currentFPS, maxFPS, minFPS, avgFPS;
    private Text text;
    private float timeBetweenUpdates = 0.2f, currentTime;
    private int lastCount;
    void Start()
    {
        text = GetComponent<Text>();
        text.text = 0.ToString();
    }
	// Update is called once per frame
	void Update () {
        currentTime += Time.deltaTime;
        if (currentTime > timeBetweenUpdates)
        {
            float pct = timeBetweenUpdates / 1.0f;
            text.text = Mathf.FloorToInt((Mathf.Abs(lastCount - Time.frameCount) * (1.0f/pct))).ToString();
            lastCount = Time.frameCount;
            currentTime = 0.0f;
        }
	}
}
