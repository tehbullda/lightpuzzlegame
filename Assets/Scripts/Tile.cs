﻿using UnityEngine;
using System.Collections;
using System.Linq;
#if UNITY_EDITOR
using UnityEditor;
#endif
public class Tile : MonoBehaviour
{
    public GridHandler.TileType type;
    public bool locked;
    TileInformationResource tileInfoResource;

    private GameObject currentTile = null;

    public void UpdateTile()
    {
        if (tileInfoResource == null)
            tileInfoResource = Globals.TileResources;

        if (type == GridHandler.TileType.None)
        {
            var rs = GetComponentsInChildren<Renderer>();
            foreach (var r in rs)
            {
                r.enabled = false;
            }
        }
        else
        {
            var rs = GetComponentsInChildren<Renderer>();
            foreach (var r in rs)
            {
                r.enabled = true;
            }
            if (tileInfoResource.tilePrefabs.ContainsKey(type))
            {
                if (tileInfoResource.tilePrefabs[type] == null)
                {
                    Debug.LogError("TileInfo Resource does not contain a Material for the type " + type.ToString());
                    return;
                }
                if (currentTile != null)
                {
                    DestroyImmediate(currentTile);
                }
                currentTile = Instantiate(tileInfoResource.tilePrefabs[type], transform);
            }
            else
            {
                Debug.LogError("TileInfo Resource does not contain the key for " + type.ToString());
            }
        }
        
    }
    public OrbMovement.MoveDir GetOutDir(OrbMovement.MoveDir inDir)
    {
        switch (type)
        {
            case GridHandler.TileType.Blocked:
                return OrbMovement.InvertedDir(inDir);
            case GridHandler.TileType.Default:
            case GridHandler.TileType.Hazard:
            case GridHandler.TileType.None:
                return inDir;
            //BACK SLASH
            case GridHandler.TileType.BCKSlashDiag:
                if (inDir == OrbMovement.MoveDir.Up)
                    return OrbMovement.MoveDir.Left;
                else if (inDir == OrbMovement.MoveDir.Down)
                    return OrbMovement.MoveDir.Right;
                else if (inDir == OrbMovement.MoveDir.Left)
                {

                    return OrbMovement.MoveDir.Up;
                }
                else
                    return OrbMovement.MoveDir.Down;
            // FORWARD SLASH
            case GridHandler.TileType.FWDSlashDiag:
                if (inDir == OrbMovement.MoveDir.Up)
                    return OrbMovement.MoveDir.Right;
                else if (inDir == OrbMovement.MoveDir.Down)
                    return OrbMovement.MoveDir.Left;
                else if (inDir == OrbMovement.MoveDir.Left)
                {
                    return OrbMovement.MoveDir.Down;
                }
                else
                    return OrbMovement.MoveDir.Up;
            // LEFT DOWN PIPE
            case GridHandler.TileType.LDPipe:
                if (inDir == OrbMovement.MoveDir.Up)
                    return OrbMovement.MoveDir.Left;
                else if (inDir == OrbMovement.MoveDir.Down)
                    return OrbMovement.MoveDir.Up;
                else if (inDir == OrbMovement.MoveDir.Left)
                    return OrbMovement.MoveDir.Right;
                else
                    return OrbMovement.MoveDir.Down;
            // RIGHT DOWN PIPE
            case GridHandler.TileType.RDPipe:
                if (inDir == OrbMovement.MoveDir.Up)
                    return OrbMovement.MoveDir.Right;
                else if (inDir == OrbMovement.MoveDir.Down)
                    return OrbMovement.MoveDir.Up;
                else if (inDir == OrbMovement.MoveDir.Left)
                    return OrbMovement.MoveDir.Down;
                else
                    return OrbMovement.MoveDir.Left;
            // UP LEFT PIPE
            case GridHandler.TileType.ULPipe:
                if (inDir == OrbMovement.MoveDir.Up)
                    return OrbMovement.MoveDir.Down;
                else if (inDir == OrbMovement.MoveDir.Down)
                    return OrbMovement.MoveDir.Left;
                else if (inDir == OrbMovement.MoveDir.Left)
                    return OrbMovement.MoveDir.Right;
                else
                    return OrbMovement.MoveDir.Up;
            // UP RIGHT PIPE
            case GridHandler.TileType.URPipe:
                if (inDir == OrbMovement.MoveDir.Up)
                    return OrbMovement.MoveDir.Down;
                else if (inDir == OrbMovement.MoveDir.Down)
                    return OrbMovement.MoveDir.Right;
                else if (inDir == OrbMovement.MoveDir.Left)
                    return OrbMovement.MoveDir.Up;
                else
                    return OrbMovement.MoveDir.Left;
            //SHOULD NEVER HAPPEN
            default:
                return OrbMovement.MoveDir.None;
        }
    }

    public void SetMatSelected()
    {
        GetComponent<Renderer>().material.SetFloat("_Blocked", 0.8f);
    }
    public void SetMatDefault()
    {
        GetComponent<Renderer>().material.SetFloat("_Blocked", 1f);
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "MainOrb")
        {
            var orbMovement = other.GetComponentInParent<OrbMovement>();
            if (type == GridHandler.TileType.Hazard)
            {
                StartCoroutine(orbMovement.Kill(transform.position + Globals.orbSpawnOffset));
            }
            orbMovement.SetNextDirection(GetOutDir(orbMovement.GetCurrentDir()), true, transform.position + Globals.orbSpawnOffset);
        }
    }
}
#if UNITY_EDITOR
[CustomEditor(typeof(Tile)), CanEditMultipleObjects]
public class Applier : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        if (GUILayout.Button("Apply"))
        {
            for (int i = 0; i < targets.Length; ++i)
            {
                Tile script = (Tile)targets[i];
                script.UpdateTile();
            }
        }
    }
}
#endif
