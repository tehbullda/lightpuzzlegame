﻿using UnityEngine;
using System.Collections;

public class Orbspawner : MonoBehaviour {

    public GameObject orbPrefab;
    public Vector3 spawnDir;
    public Rect restraints;
    private GameObject currentSpawned;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	    if (Input.GetKeyDown(KeyCode.Space))
        {
            SpawnOrb();
        }
        if (currentSpawned)
        {
            if (currentSpawned.transform.position.x < restraints.x || currentSpawned.transform.position.x > restraints.width || currentSpawned.transform.position.z < restraints.y || currentSpawned.transform.position.z > restraints.height)
            {
                ToggleOrb(false);
            }
        }
	}
    public void ToggleOrb(bool toggleVal)
    {
        if (toggleVal)
        {
            SpawnOrb();
        }
        else
        {
            if (currentSpawned)
            {
                Destroy(currentSpawned);
                currentSpawned = null;
                Globals.OrbInPlay = false;
            }
        }
    }
    private void SpawnOrb()
    {
        if (currentSpawned != null)
        {
            Destroy(currentSpawned);
        }
        currentSpawned = (GameObject)Instantiate(orbPrefab, transform.position + Globals.orbSpawnOffset, Quaternion.identity);
        currentSpawned.GetComponent<OrbMovement>().SetNextDirection(spawnDir);
        currentSpawned.GetComponent<OrbMovement>().spawner = this;
        Globals.OrbInPlay = true;
    }
}
