﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class SingletonBehaviour<T> : MonoBehaviour where T : SingletonBehaviour<T>
{
    public static bool AssertInstance()
    {
        if (!Application.isPlaying)
        {
            return false;
        }
        if (Instance == null)
        {
            return false;
        }
        return true;
    }

    public static bool InstanceExists()
    {
        return _instance != null;
    }

    protected virtual void Awake()
    {
        if (_instance == null)
        {
            _instance = (T)this;
            initializable = true;
        }
        else
        {
            initializable = false;
            duplicateObject = true;
        }

        if (duplicateObject)
        {
            RemoveDuplicateObject();
        }
        if (initializable)
        {
            Init();
            if (OnInitialized != null)
            {
                OnInitialized();
            }
        }
    }
    protected Action OnInitialized;
    protected bool initializable;
    protected bool duplicateObject;
    protected virtual void Init()
    {

    }

    static protected T _instance;
    static public T Instance
    {
        get
        {
            if (_instance == null)
            {
                //Debug.Log("<Color=cyan>_SingleTon is null</color> <color=white> " + typeof(T).FullName + "</color>");

                var singletons = GameObject.FindObjectsOfType<T>();
                if (singletons != null)
                {
                    if (singletons.Length >= 1)
                    {
                        _instance = singletons[0];
                        if (singletons.Length > 1)
                        {
                            for (int i = 1; i < singletons.Length; i++)
                            {
                                singletons[i].RemoveDuplicateObject();
                            }
                        }
                    }
                }
                if (_instance == null)
                {
                    GameObject g = new GameObject("_" + typeof(T).Name);
                    g.AddComponent<T>();
                    //Debug.Log("<Color=cyan>_SingleTon Created</color> <color=white> "+typeof(T).FullName+"</color>");
                }
                else
                {
                    _instance.name = "_" + typeof(T).FullName;
                    //Debug.Log("<Color=cyan>_SingleTon Found</color> <color=white> " + typeof(T).FullName + "</color>");

                }
            }
            else
            {
                // Debug.Log("<Color=cyan>_Returning singleton </color> <color=white> " + typeof(T).FullName + "</color>");
                if (!_instance.gameObject.activeInHierarchy)
                {
                    _instance.gameObject.SetActive(true);
                }
            }
            return _instance;
        }
    }

    public void RemoveDuplicateObject()
    {
        //Debug.Log("Removing duplicate object " + typeof(T).FullName);
        var components = gameObject.GetComponents<MonoBehaviour>();
        if (components.Length == 1)
        {
            Destroy(gameObject);
        }
        else
        {
            Destroy(this);
            gameObject.name = "_unnamedSingleton";
        }

    }
}