﻿using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif
public class Level : MonoBehaviour
{

    public int sizeX, sizeY;
    private Orbspawner spawner;
    public Vector3 startPos;
    public Vector3 offsetFromStart;
    public GameObject cubePrefab, startPrefab, goalPrefab;
    public float offsetBetweenCubes;

    [SerializeField]
    private static GameObject[] tilePrefabs;

    void Start()
    {
        spawner = GetComponentInChildren<Orbspawner>();
    }
    public void ToggleOrbSpawn(bool toggleVal)
    {
        spawner.ToggleOrb(toggleVal);
    }
    public void MoveToStart()
    {
        transform.position = new Vector3(0,0,0);
    }
    public void MoveDown()
    {
        transform.position = new Vector3(0,-10,0);
    }
    public void Populate()
    {
        MoveToStart();
        int childCount = transform.childCount;
        for (int i = childCount - 1; i >= 0; --i)
        {
            DestroyImmediate(transform.GetChild(i).gameObject);
        }

        FillLevel(sizeX, sizeY);
    }
    public void Procedural()
    {
        MoveToStart();
        int childCount = transform.childCount;
        for (int i = childCount - 1; i >= 0; --i)
        {
            DestroyImmediate(transform.GetChild(i).gameObject);
        }
        FillLevel(sizeX, sizeY, true);
    }
    public void FillLevel(int sizeX, int sizeY, bool randomTiles = false)
    {
        Transform levelParent = transform.parent;
        Vector3 currentPos = startPos + offsetFromStart;
        GameObject tmp = Instantiate(startPrefab, new Vector3(-7, 0, -7), Quaternion.identity, transform);
        tmp.name = "StartCube";
        tmp = Instantiate(goalPrefab, new Vector3(3.5f, 0, 3.75f), Quaternion.identity, transform);
        tmp.name = "GoalCube";
        for (int i = 0; i < sizeY; ++i)
        {
            for (int j = 0; j < sizeX; ++j)
            {
                tmp = Instantiate(cubePrefab, currentPos, Quaternion.identity, transform);
                tmp.name = "Cube " + i + ":" + j;
                Tile script = tmp.GetComponent<Tile>();
                script.type = !randomTiles ? GridHandler.TileType.Default : (GridHandler.TileType)Random.Range(0, (int)GridHandler.TileType.Count);
                //script.UpdateMat();
                currentPos.z += offsetBetweenCubes;
            }
            currentPos.x += offsetBetweenCubes;
            currentPos.z = startPos.z + offsetFromStart.z;
        }
    }
    public void Save()
    {
        PlayerPrefs.SetInt(Globals.levelSaveBaseStr + Globals.currentLevel, 1);
        PlayerPrefs.Save();
    }
}
#if UNITY_EDITOR
[CustomEditor(typeof(Level)), CanEditMultipleObjects]
public class Generator : Editor
{
    bool confirmed = false;
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        Level script = (Level)target;
        GUILayout.Space(15.0f);
        GUILayout.Label("THIS WILL RESET EVERYTHING IN THE LEVEL");
        if (GUILayout.Button("Generate Tiles") && confirmed)
        {
            script.Populate();
        }
        if (GUILayout.Button("Procedural"))
        {
            script.Procedural();
        }
        GUILayout.Space(15.0f);
        confirmed = GUILayout.Toggle(confirmed, "I HEREBY ACCEPT THAT THE BUTTON WILL RESET EVERYTHING IN THE LEVEL");
    }
}
#endif