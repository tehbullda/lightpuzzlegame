﻿using UnityEngine;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine.UI;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif
public class GridHandler : SimpleSingletonBehaviour<GridHandler>
{
    public enum TileType
    {
        Default,
        Blocked,
        Hazard,
        FWDSlashDiag,
        BCKSlashDiag,
        LDPipe,
        RDPipe,
        URPipe,
        ULPipe,
        None,
        Count
    }
    public GameObject LevelPackPrefab;

    private Level currentActiveLevel;
    private LevelPack currentActivePack;

    void Start()
    {
        StartCoroutine(InitializeLevel());
    }
    private IEnumerator InitializeLevel()
    {
        yield return LoadLevelBundle();
        currentActivePack.ReloadLevels();
        LoadLevel();
    }
    public void LoadNextLevel()
    {
        Globals.currentLevel++;
        LoadLevel();
    }
    public void LoadLevel()
    {
        currentActiveLevel = currentActivePack.LoadLevel();
    }
    public void SendSpawnCommand(bool spawn)
    {
        currentActiveLevel.ToggleOrbSpawn(spawn);
    }
    public void ResetOrb()
    {
        currentActiveLevel.ToggleOrbSpawn(false);
    }
    private IEnumerator LoadLevelBundle()
    {
        int packRequested = Globals.currentLevel == 0 ? 1 : Mathf.CeilToInt(Globals.currentLevel / 5f);
        string filePath = System.IO.Path.Combine(Application.streamingAssetsPath, "levels/" + packRequested);
        AssetBundleCreateRequest req = AssetBundle.LoadFromFileAsync(filePath);
        yield return req;
        if (req.assetBundle == null)
        {
            Debug.Log("Failed to load AssetBundle!");
            yield return null;
        }
        var prefab = req.assetBundle.LoadAsset<GameObject>("LevelPack " + packRequested);
        currentActivePack = Instantiate(prefab, transform).GetComponent<LevelPack>();
        currentActivePack.gameObject.SetActive(true);
    }
}
#if UNITY_EDITOR
[CustomEditor(typeof(GridHandler))]
class LevelPackEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        GridHandler script = target as GridHandler;
        if (GUILayout.Button("New LevelPack") && script.LevelPackPrefab != null)
        {
            GameObject tmp = Instantiate(script.LevelPackPrefab, script.transform, false);
            tmp.name = "LevelPack " + script.transform.childCount;
        }
        
    }
}
#endif
