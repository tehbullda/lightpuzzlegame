﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TogglePlayButton : MonoBehaviour
{    
    public Toggle toggle;
    public GridHandler gridHandler;
    private bool ignoreOnce = false;
    void Start()
    {
        toggle = GetComponent<Toggle>();
        toggle.onValueChanged.RemoveAllListeners();
        toggle.onValueChanged.AddListener((b) =>
        {
            if (!ignoreOnce)
            {
                gridHandler.SendSpawnCommand(b);
            }
            ignoreOnce = false;
        });
        Globals.OrbAction += OrbAction;
    }
    private void OnDestroy()
    {
        Globals.OrbAction -= OrbAction;
    }
    private void OrbAction(bool b)
    {
        ignoreOnce = true;
        toggle.isOn = b;
    }
}
