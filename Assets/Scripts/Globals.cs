﻿using UnityEngine;
using System.Collections;
using System;

public static class Globals
{
    public static int currentLevel;
    public const string levelSaveBaseStr = "Level ";
    private static bool orbInPlay;
    public static bool OrbInPlay 
    {
        set { orbInPlay = value; OrbAction?.Invoke(value); }
        get { return orbInPlay; }
    }
    public static Vector3 orbSpawnOffset = new Vector3(0, 2, 0);
    public static Action<bool> OrbAction;

    private static TileInformationResource tileResources = null;
    public static TileInformationResource TileResources
    {
        get
        {
            if (tileResources == null)
            {
                tileResources = Resources.Load<TileInformationResource>("TileInfo");
            }
            return tileResources;
        }
    }
}
