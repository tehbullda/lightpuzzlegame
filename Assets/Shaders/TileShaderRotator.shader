// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Shader created with Shader Forge v1.30 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.30;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:1,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:2865,x:32850,y:32708,varname:node_2865,prsc:2|diff-2404-OUT,spec-9424-OUT,gloss-6032-OUT,emission-3386-OUT;n:type:ShaderForge.SFN_Multiply,id:6343,x:31770,y:32328,varname:node_6343,prsc:2|A-7736-RGB,B-6665-RGB;n:type:ShaderForge.SFN_Color,id:6665,x:31470,y:32424,ptovrint:False,ptlb:TileColor,ptin:_TileColor,varname:_Color,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5019608,c2:0.5019608,c3:0.5019608,c4:1;n:type:ShaderForge.SFN_Tex2d,id:7736,x:31470,y:32242,ptovrint:True,ptlb:TileTexture,ptin:_MainTex,varname:_MainTex,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:964f410351092bd419fea49b29fa3712,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:5812,x:31312,y:33114,ptovrint:False,ptlb:Arrow1,ptin:_Arrow1,varname:node_5812,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False|UVIN-3026-UVOUT;n:type:ShaderForge.SFN_Color,id:9169,x:32089,y:33558,ptovrint:False,ptlb:EmissiveColor,ptin:_EmissiveColor,varname:node_9169,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Multiply,id:7706,x:32348,y:33017,varname:node_7706,prsc:2|A-8599-OUT,B-9169-RGB;n:type:ShaderForge.SFN_Power,id:3386,x:32617,y:33017,varname:node_3386,prsc:2|VAL-7706-OUT,EXP-6042-OUT;n:type:ShaderForge.SFN_Vector1,id:6042,x:32255,y:33255,varname:node_6042,prsc:2,v1:2;n:type:ShaderForge.SFN_ValueProperty,id:1392,x:31187,y:32861,ptovrint:False,ptlb:Blocked,ptin:_Blocked,varname:node_1392,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_Multiply,id:727,x:32050,y:32282,varname:node_727,prsc:2|A-6343-OUT,B-1392-OUT;n:type:ShaderForge.SFN_Vector1,id:9424,x:32665,y:32747,varname:node_9424,prsc:2,v1:0;n:type:ShaderForge.SFN_Lerp,id:2404,x:32484,y:32483,varname:node_2404,prsc:2|A-727-OUT,B-9169-RGB,T-8599-OUT;n:type:ShaderForge.SFN_Vector1,id:6032,x:32665,y:32801,varname:node_6032,prsc:2,v1:1;n:type:ShaderForge.SFN_Rotator,id:8255,x:31763,y:32669,varname:node_8255,prsc:2|UVIN-4668-UVOUT,ANG-2861-OUT;n:type:ShaderForge.SFN_Tex2d,id:462,x:31986,y:32692,ptovrint:False,ptlb:EmissiveCog,ptin:_EmissiveCog,varname:node_462,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False|UVIN-8255-UVOUT;n:type:ShaderForge.SFN_TexCoord,id:4668,x:31410,y:32756,varname:node_4668,prsc:2,uv:0;n:type:ShaderForge.SFN_ValueProperty,id:2861,x:31579,y:32800,ptovrint:False,ptlb:CogRotation,ptin:_CogRotation,varname:node_2861,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;n:type:ShaderForge.SFN_Add,id:8599,x:32170,y:32862,varname:node_8599,prsc:2|A-462-RGB,B-9511-OUT;n:type:ShaderForge.SFN_Tex2d,id:3859,x:31312,y:33304,ptovrint:False,ptlb:Arrow2,ptin:_Arrow2,varname:_EmissiveArrow_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False|UVIN-6265-UVOUT;n:type:ShaderForge.SFN_Tex2d,id:9661,x:31312,y:33498,ptovrint:False,ptlb:Arrow3,ptin:_Arrow3,varname:_EmissiveArrow3,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False|UVIN-5765-UVOUT;n:type:ShaderForge.SFN_Tex2d,id:8416,x:31312,y:33690,ptovrint:False,ptlb:Arrow4,ptin:_Arrow4,varname:_EmissiveArrow4,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False|UVIN-9656-UVOUT;n:type:ShaderForge.SFN_TexCoord,id:5168,x:30586,y:33181,varname:node_5168,prsc:2,uv:0;n:type:ShaderForge.SFN_Rotator,id:3026,x:31115,y:33131,varname:node_3026,prsc:2|UVIN-5168-UVOUT,ANG-3075-OUT;n:type:ShaderForge.SFN_Rotator,id:6265,x:31115,y:33304,varname:node_6265,prsc:2|UVIN-5168-UVOUT,ANG-1940-OUT;n:type:ShaderForge.SFN_Rotator,id:5765,x:31139,y:33498,varname:node_5765,prsc:2|UVIN-5168-UVOUT,ANG-2936-OUT;n:type:ShaderForge.SFN_Rotator,id:9656,x:31128,y:33690,varname:node_9656,prsc:2|UVIN-5168-UVOUT,ANG-6560-OUT;n:type:ShaderForge.SFN_Vector1,id:6560,x:30950,y:33761,varname:node_6560,prsc:2,v1:4.71;n:type:ShaderForge.SFN_Vector1,id:2936,x:30887,y:33574,varname:node_2936,prsc:2,v1:3.14;n:type:ShaderForge.SFN_Vector1,id:1940,x:30917,y:33369,varname:node_1940,prsc:2,v1:1.57;n:type:ShaderForge.SFN_Vector1,id:3075,x:30959,y:33186,varname:node_3075,prsc:2,v1:0;n:type:ShaderForge.SFN_ValueProperty,id:7031,x:31632,y:33364,ptovrint:False,ptlb:ArrowOff,ptin:_ArrowOff,varname:node_7031,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.5;n:type:ShaderForge.SFN_Add,id:7165,x:31617,y:33179,varname:node_7165,prsc:2|A-5812-RGB,B-3859-RGB,C-9661-RGB,D-8416-RGB;n:type:ShaderForge.SFN_Multiply,id:7052,x:31815,y:33143,varname:node_7052,prsc:2|A-7165-OUT,B-7031-OUT;n:type:ShaderForge.SFN_Tex2d,id:6551,x:31689,y:33479,ptovrint:False,ptlb:ArrowRotate,ptin:_ArrowRotate,varname:node_6551,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False|UVIN-8578-UVOUT;n:type:ShaderForge.SFN_TexCoord,id:240,x:31494,y:33762,varname:node_240,prsc:2,uv:0;n:type:ShaderForge.SFN_Rotator,id:8578,x:31733,y:33728,varname:node_8578,prsc:2|UVIN-240-UVOUT,ANG-8172-OUT;n:type:ShaderForge.SFN_ValueProperty,id:7971,x:31211,y:34033,ptovrint:False,ptlb:ArrowRotation,ptin:_ArrowRotation,varname:node_7971,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;n:type:ShaderForge.SFN_Multiply,id:8172,x:31437,y:34068,varname:node_8172,prsc:2|A-7971-OUT,B-3273-OUT;n:type:ShaderForge.SFN_Vector1,id:3273,x:30952,y:34170,varname:node_3273,prsc:2,v1:-1.57;n:type:ShaderForge.SFN_Multiply,id:9484,x:31908,y:33389,varname:node_9484,prsc:2|A-7866-OUT,B-6551-RGB;n:type:ShaderForge.SFN_Add,id:9511,x:31995,y:33104,varname:node_9511,prsc:2|A-7052-OUT,B-9484-OUT;n:type:ShaderForge.SFN_Vector1,id:7866,x:31742,y:33399,varname:node_7866,prsc:2,v1:0.5;proporder:6665-7736-5812-9169-1392-462-2861-3859-9661-8416-7031-6551-7971;pass:END;sub:END;*/

Shader "Shader Forge/TileShaderRotator" {
    Properties {
        _TileColor ("TileColor", Color) = (0.5019608,0.5019608,0.5019608,1)
        _MainTex ("TileTexture", 2D) = "white" {}
        _Arrow1 ("Arrow1", 2D) = "white" {}
        _EmissiveColor ("EmissiveColor", Color) = (0.5,0.5,0.5,1)
        _Blocked ("Blocked", Float ) = 1
        _EmissiveCog ("EmissiveCog", 2D) = "white" {}
        _CogRotation ("CogRotation", Float ) = 0
        _Arrow2 ("Arrow2", 2D) = "white" {}
        _Arrow3 ("Arrow3", 2D) = "white" {}
        _Arrow4 ("Arrow4", 2D) = "white" {}
        _ArrowOff ("ArrowOff", Float ) = 0.5
        _ArrowRotate ("ArrowRotate", 2D) = "white" {}
        _ArrowRotation ("ArrowRotation", Float ) = 0
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform float4 _TileColor;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform sampler2D _Arrow1; uniform float4 _Arrow1_ST;
            uniform float4 _EmissiveColor;
            uniform float _Blocked;
            uniform sampler2D _EmissiveCog; uniform float4 _EmissiveCog_ST;
            uniform float _CogRotation;
            uniform sampler2D _Arrow2; uniform float4 _Arrow2_ST;
            uniform sampler2D _Arrow3; uniform float4 _Arrow3_ST;
            uniform sampler2D _Arrow4; uniform float4 _Arrow4_ST;
            uniform float _ArrowOff;
            uniform sampler2D _ArrowRotate; uniform float4 _ArrowRotate_ST;
            uniform float _ArrowRotation;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                LIGHTING_COORDS(3,4)
                UNITY_FOG_COORDS(5)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos(v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
///////// Gloss:
                float gloss = 1.0 - 1.0; // Convert roughness to gloss
                float specPow = exp2( gloss * 10.0+1.0);
////// Specular:
                float NdotL = max(0, dot( normalDirection, lightDirection ));
                float node_9424 = 0.0;
                float3 specularColor = float3(node_9424,node_9424,node_9424);
                float3 directSpecular = (floor(attenuation) * _LightColor0.xyz) * pow(max(0,dot(halfDirection,normalDirection)),specPow)*specularColor;
                float3 specular = directSpecular;
/////// Diffuse:
                NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float3 directDiffuse = max( 0.0, NdotL) * attenColor;
                float3 indirectDiffuse = float3(0,0,0);
                indirectDiffuse += UNITY_LIGHTMODEL_AMBIENT.rgb; // Ambient Light
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float node_8255_ang = _CogRotation;
                float node_8255_spd = 1.0;
                float node_8255_cos = cos(node_8255_spd*node_8255_ang);
                float node_8255_sin = sin(node_8255_spd*node_8255_ang);
                float2 node_8255_piv = float2(0.5,0.5);
                float2 node_8255 = (mul(i.uv0-node_8255_piv,float2x2( node_8255_cos, -node_8255_sin, node_8255_sin, node_8255_cos))+node_8255_piv);
                float4 _EmissiveCog_var = tex2D(_EmissiveCog,TRANSFORM_TEX(node_8255, _EmissiveCog));
                float node_3026_ang = 0.0;
                float node_3026_spd = 1.0;
                float node_3026_cos = cos(node_3026_spd*node_3026_ang);
                float node_3026_sin = sin(node_3026_spd*node_3026_ang);
                float2 node_3026_piv = float2(0.5,0.5);
                float2 node_3026 = (mul(i.uv0-node_3026_piv,float2x2( node_3026_cos, -node_3026_sin, node_3026_sin, node_3026_cos))+node_3026_piv);
                float4 _Arrow1_var = tex2D(_Arrow1,TRANSFORM_TEX(node_3026, _Arrow1));
                float node_6265_ang = 1.57;
                float node_6265_spd = 1.0;
                float node_6265_cos = cos(node_6265_spd*node_6265_ang);
                float node_6265_sin = sin(node_6265_spd*node_6265_ang);
                float2 node_6265_piv = float2(0.5,0.5);
                float2 node_6265 = (mul(i.uv0-node_6265_piv,float2x2( node_6265_cos, -node_6265_sin, node_6265_sin, node_6265_cos))+node_6265_piv);
                float4 _Arrow2_var = tex2D(_Arrow2,TRANSFORM_TEX(node_6265, _Arrow2));
                float node_5765_ang = 3.14;
                float node_5765_spd = 1.0;
                float node_5765_cos = cos(node_5765_spd*node_5765_ang);
                float node_5765_sin = sin(node_5765_spd*node_5765_ang);
                float2 node_5765_piv = float2(0.5,0.5);
                float2 node_5765 = (mul(i.uv0-node_5765_piv,float2x2( node_5765_cos, -node_5765_sin, node_5765_sin, node_5765_cos))+node_5765_piv);
                float4 _Arrow3_var = tex2D(_Arrow3,TRANSFORM_TEX(node_5765, _Arrow3));
                float node_9656_ang = 4.71;
                float node_9656_spd = 1.0;
                float node_9656_cos = cos(node_9656_spd*node_9656_ang);
                float node_9656_sin = sin(node_9656_spd*node_9656_ang);
                float2 node_9656_piv = float2(0.5,0.5);
                float2 node_9656 = (mul(i.uv0-node_9656_piv,float2x2( node_9656_cos, -node_9656_sin, node_9656_sin, node_9656_cos))+node_9656_piv);
                float4 _Arrow4_var = tex2D(_Arrow4,TRANSFORM_TEX(node_9656, _Arrow4));
                float node_3273 = (-1.57);
                float node_8172 = (_ArrowRotation*node_3273);
                float node_8578_ang = node_8172;
                float node_8578_spd = 1.0;
                float node_8578_cos = cos(node_8578_spd*node_8578_ang);
                float node_8578_sin = sin(node_8578_spd*node_8578_ang);
                float2 node_8578_piv = float2(0.5,0.5);
                float2 node_8578 = (mul(i.uv0-node_8578_piv,float2x2( node_8578_cos, -node_8578_sin, node_8578_sin, node_8578_cos))+node_8578_piv);
                float4 _ArrowRotate_var = tex2D(_ArrowRotate,TRANSFORM_TEX(node_8578, _ArrowRotate));
                float3 node_8599 = (_EmissiveCog_var.rgb+(((_Arrow1_var.rgb+_Arrow2_var.rgb+_Arrow3_var.rgb+_Arrow4_var.rgb)*_ArrowOff)+(0.5*_ArrowRotate_var.rgb)));
                float3 diffuseColor = lerp(((_MainTex_var.rgb*_TileColor.rgb)*_Blocked),_EmissiveColor.rgb,node_8599);
                float3 diffuse = (directDiffuse + indirectDiffuse) * diffuseColor;
////// Emissive:
                float3 emissive = pow((node_8599*_EmissiveColor.rgb),2.0);
/// Final Color:
                float3 finalColor = diffuse + specular + emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "FORWARD_DELTA"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdadd_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform float4 _TileColor;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform sampler2D _Arrow1; uniform float4 _Arrow1_ST;
            uniform float4 _EmissiveColor;
            uniform float _Blocked;
            uniform sampler2D _EmissiveCog; uniform float4 _EmissiveCog_ST;
            uniform float _CogRotation;
            uniform sampler2D _Arrow2; uniform float4 _Arrow2_ST;
            uniform sampler2D _Arrow3; uniform float4 _Arrow3_ST;
            uniform sampler2D _Arrow4; uniform float4 _Arrow4_ST;
            uniform float _ArrowOff;
            uniform sampler2D _ArrowRotate; uniform float4 _ArrowRotate_ST;
            uniform float _ArrowRotation;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                LIGHTING_COORDS(3,4)
                UNITY_FOG_COORDS(5)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos(v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
///////// Gloss:
                float gloss = 1.0 - 1.0; // Convert roughness to gloss
                float specPow = exp2( gloss * 10.0+1.0);
////// Specular:
                float NdotL = max(0, dot( normalDirection, lightDirection ));
                float node_9424 = 0.0;
                float3 specularColor = float3(node_9424,node_9424,node_9424);
                float3 directSpecular = attenColor * pow(max(0,dot(halfDirection,normalDirection)),specPow)*specularColor;
                float3 specular = directSpecular;
/////// Diffuse:
                NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float3 directDiffuse = max( 0.0, NdotL) * attenColor;
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float node_8255_ang = _CogRotation;
                float node_8255_spd = 1.0;
                float node_8255_cos = cos(node_8255_spd*node_8255_ang);
                float node_8255_sin = sin(node_8255_spd*node_8255_ang);
                float2 node_8255_piv = float2(0.5,0.5);
                float2 node_8255 = (mul(i.uv0-node_8255_piv,float2x2( node_8255_cos, -node_8255_sin, node_8255_sin, node_8255_cos))+node_8255_piv);
                float4 _EmissiveCog_var = tex2D(_EmissiveCog,TRANSFORM_TEX(node_8255, _EmissiveCog));
                float node_3026_ang = 0.0;
                float node_3026_spd = 1.0;
                float node_3026_cos = cos(node_3026_spd*node_3026_ang);
                float node_3026_sin = sin(node_3026_spd*node_3026_ang);
                float2 node_3026_piv = float2(0.5,0.5);
                float2 node_3026 = (mul(i.uv0-node_3026_piv,float2x2( node_3026_cos, -node_3026_sin, node_3026_sin, node_3026_cos))+node_3026_piv);
                float4 _Arrow1_var = tex2D(_Arrow1,TRANSFORM_TEX(node_3026, _Arrow1));
                float node_6265_ang = 1.57;
                float node_6265_spd = 1.0;
                float node_6265_cos = cos(node_6265_spd*node_6265_ang);
                float node_6265_sin = sin(node_6265_spd*node_6265_ang);
                float2 node_6265_piv = float2(0.5,0.5);
                float2 node_6265 = (mul(i.uv0-node_6265_piv,float2x2( node_6265_cos, -node_6265_sin, node_6265_sin, node_6265_cos))+node_6265_piv);
                float4 _Arrow2_var = tex2D(_Arrow2,TRANSFORM_TEX(node_6265, _Arrow2));
                float node_5765_ang = 3.14;
                float node_5765_spd = 1.0;
                float node_5765_cos = cos(node_5765_spd*node_5765_ang);
                float node_5765_sin = sin(node_5765_spd*node_5765_ang);
                float2 node_5765_piv = float2(0.5,0.5);
                float2 node_5765 = (mul(i.uv0-node_5765_piv,float2x2( node_5765_cos, -node_5765_sin, node_5765_sin, node_5765_cos))+node_5765_piv);
                float4 _Arrow3_var = tex2D(_Arrow3,TRANSFORM_TEX(node_5765, _Arrow3));
                float node_9656_ang = 4.71;
                float node_9656_spd = 1.0;
                float node_9656_cos = cos(node_9656_spd*node_9656_ang);
                float node_9656_sin = sin(node_9656_spd*node_9656_ang);
                float2 node_9656_piv = float2(0.5,0.5);
                float2 node_9656 = (mul(i.uv0-node_9656_piv,float2x2( node_9656_cos, -node_9656_sin, node_9656_sin, node_9656_cos))+node_9656_piv);
                float4 _Arrow4_var = tex2D(_Arrow4,TRANSFORM_TEX(node_9656, _Arrow4));
                float node_3273 = (-1.57);
                float node_8172 = (_ArrowRotation*node_3273);
                float node_8578_ang = node_8172;
                float node_8578_spd = 1.0;
                float node_8578_cos = cos(node_8578_spd*node_8578_ang);
                float node_8578_sin = sin(node_8578_spd*node_8578_ang);
                float2 node_8578_piv = float2(0.5,0.5);
                float2 node_8578 = (mul(i.uv0-node_8578_piv,float2x2( node_8578_cos, -node_8578_sin, node_8578_sin, node_8578_cos))+node_8578_piv);
                float4 _ArrowRotate_var = tex2D(_ArrowRotate,TRANSFORM_TEX(node_8578, _ArrowRotate));
                float3 node_8599 = (_EmissiveCog_var.rgb+(((_Arrow1_var.rgb+_Arrow2_var.rgb+_Arrow3_var.rgb+_Arrow4_var.rgb)*_ArrowOff)+(0.5*_ArrowRotate_var.rgb)));
                float3 diffuseColor = lerp(((_MainTex_var.rgb*_TileColor.rgb)*_Blocked),_EmissiveColor.rgb,node_8599);
                float3 diffuse = directDiffuse * diffuseColor;
/// Final Color:
                float3 finalColor = diffuse + specular;
                fixed4 finalRGBA = fixed4(finalColor * 1,0);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
