// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Shader created with Shader Forge v1.28 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.28;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:0,dpts:2,wrdp:False,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:True,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:4795,x:32724,y:32693,varname:node_4795,prsc:2|emission-4333-OUT;n:type:ShaderForge.SFN_Tex2d,id:6074,x:31967,y:32173,ptovrint:False,ptlb:SmokeMask1,ptin:_SmokeMask1,varname:_MainTex,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:158c813a896068f4c9f500bbb4ae5314,ntxv:0,isnm:False|UVIN-6249-UVOUT;n:type:ShaderForge.SFN_Multiply,id:2393,x:32434,y:32766,varname:node_2393,prsc:2|A-8699-OUT,B-2053-RGB,C-797-RGB,D-9248-OUT;n:type:ShaderForge.SFN_VertexColor,id:2053,x:32194,y:32748,varname:node_2053,prsc:2;n:type:ShaderForge.SFN_Color,id:797,x:32051,y:32887,ptovrint:True,ptlb:Color,ptin:_TintColor,varname:_TintColor,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Vector1,id:9248,x:32051,y:33058,varname:node_9248,prsc:2,v1:1;n:type:ShaderForge.SFN_Panner,id:6249,x:31750,y:32227,varname:node_6249,prsc:2,spu:0.1,spv:0.1|UVIN-858-UVOUT;n:type:ShaderForge.SFN_TexCoord,id:858,x:31453,y:32267,varname:node_858,prsc:2,uv:0;n:type:ShaderForge.SFN_Tex2d,id:4141,x:31889,y:32396,ptovrint:False,ptlb:SmokeMask2,ptin:_SmokeMask2,varname:node_4141,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:158c813a896068f4c9f500bbb4ae5314,ntxv:0,isnm:False|UVIN-2762-UVOUT;n:type:ShaderForge.SFN_Panner,id:2762,x:31687,y:32382,varname:node_2762,prsc:2,spu:-0.3,spv:-0.2|UVIN-858-UVOUT;n:type:ShaderForge.SFN_Add,id:4674,x:32151,y:32358,varname:node_4674,prsc:2|A-6074-RGB,B-4141-RGB;n:type:ShaderForge.SFN_Fresnel,id:304,x:31963,y:32589,varname:node_304,prsc:2;n:type:ShaderForge.SFN_Multiply,id:8699,x:32362,y:32529,varname:node_8699,prsc:2|A-4674-OUT,B-9366-OUT;n:type:ShaderForge.SFN_OneMinus,id:9366,x:32218,y:32589,varname:node_9366,prsc:2|IN-304-OUT;n:type:ShaderForge.SFN_ValueProperty,id:5262,x:32219,y:33187,ptovrint:False,ptlb:Opacity,ptin:_Opacity,varname:node_5262,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_Multiply,id:4333,x:32520,y:32942,varname:node_4333,prsc:2|A-2393-OUT,B-5262-OUT;proporder:6074-797-4141-5262;pass:END;sub:END;*/

Shader "Shader Forge/LightsphereShader" {
    Properties {
        _SmokeMask1 ("SmokeMask1", 2D) = "white" {}
        _TintColor ("Color", Color) = (0.5,0.5,0.5,1)
        _SmokeMask2 ("SmokeMask2", 2D) = "white" {}
        _Opacity ("Opacity", Float ) = 1
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend One One
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform sampler2D _SmokeMask1; uniform float4 _SmokeMask1_ST;
            uniform float4 _TintColor;
            uniform sampler2D _SmokeMask2; uniform float4 _SmokeMask2_ST;
            uniform float _Opacity;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float4 vertexColor : COLOR;
                UNITY_FOG_COORDS(3)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityObjectToClipPos(v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
////// Lighting:
////// Emissive:
                float4 node_7008 = _Time + _TimeEditor;
                float2 node_6249 = (i.uv0+node_7008.g*float2(0.1,0.1));
                float4 _SmokeMask1_var = tex2D(_SmokeMask1,TRANSFORM_TEX(node_6249, _SmokeMask1));
                float2 node_2762 = (i.uv0+node_7008.g*float2(-0.3,-0.2));
                float4 _SmokeMask2_var = tex2D(_SmokeMask2,TRANSFORM_TEX(node_2762, _SmokeMask2));
                float3 emissive = ((((_SmokeMask1_var.rgb+_SmokeMask2_var.rgb)*(1.0 - (1.0-max(0,dot(normalDirection, viewDirection)))))*i.vertexColor.rgb*_TintColor.rgb*1.0)*_Opacity);
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG_COLOR(i.fogCoord, finalRGBA, fixed4(0.5,0.5,0.5,1));
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
