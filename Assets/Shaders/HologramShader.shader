// Shader created with Shader Forge v1.38 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.38;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:0,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:0,dpts:2,wrdp:False,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,atwp:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:3138,x:32719,y:32712,varname:node_3138,prsc:2|emission-8592-OUT;n:type:ShaderForge.SFN_Tex2d,id:9723,x:31449,y:32597,ptovrint:False,ptlb:Hologram_Mask,ptin:_Hologram_Mask,varname:node_9723,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:7e5b4a63bb5b57a4c85c1b051171769e,ntxv:0,isnm:False|UVIN-1593-UVOUT;n:type:ShaderForge.SFN_Multiply,id:5238,x:32042,y:32662,varname:node_5238,prsc:2|A-5015-RGB,B-9723-RGB;n:type:ShaderForge.SFN_Color,id:5015,x:31777,y:32488,ptovrint:False,ptlb:Hologram_Color,ptin:_Hologram_Color,varname:node_5015,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Panner,id:1593,x:31235,y:32685,varname:node_1593,prsc:2,spu:0.2,spv:0.2|UVIN-3429-UVOUT;n:type:ShaderForge.SFN_TexCoord,id:3429,x:30956,y:32869,varname:node_3429,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_Tex2d,id:3595,x:31553,y:33086,ptovrint:False,ptlb:Hologram_Mask2,ptin:_Hologram_Mask2,varname:_Hologram_Mask_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:7e5b4a63bb5b57a4c85c1b051171769e,ntxv:0,isnm:False|UVIN-8041-UVOUT;n:type:ShaderForge.SFN_Panner,id:8041,x:31334,y:33133,varname:node_8041,prsc:2,spu:-0.2,spv:-0.2|UVIN-1415-UVOUT;n:type:ShaderForge.SFN_TexCoord,id:1415,x:31035,y:33151,varname:node_1415,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_Multiply,id:8519,x:32076,y:32890,varname:node_8519,prsc:2|A-5015-RGB,B-3595-RGB;n:type:ShaderForge.SFN_Multiply,id:8592,x:32236,y:32742,varname:node_8592,prsc:2|A-5238-OUT,B-8519-OUT;proporder:9723-5015-3595;pass:END;sub:END;*/

Shader "Shader Forge/HologramShader" {
    Properties {
        _Hologram_Mask ("Hologram_Mask", 2D) = "white" {}
        _Hologram_Color ("Hologram_Color", Color) = (0.5,0.5,0.5,1)
        _Hologram_Mask2 ("Hologram_Mask2", 2D) = "white" {}
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend One One
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform sampler2D _Hologram_Mask; uniform float4 _Hologram_Mask_ST;
            uniform float4 _Hologram_Color;
            uniform sampler2D _Hologram_Mask2; uniform float4 _Hologram_Mask2_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = UnityObjectToClipPos( v.vertex );
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
////// Lighting:
////// Emissive:
                float4 node_9192 = _Time;
                float2 node_1593 = (i.uv0+node_9192.g*float2(0.2,0.2));
                float4 _Hologram_Mask_var = tex2D(_Hologram_Mask,TRANSFORM_TEX(node_1593, _Hologram_Mask));
                float2 node_8041 = (i.uv0+node_9192.g*float2(-0.2,-0.2));
                float4 _Hologram_Mask2_var = tex2D(_Hologram_Mask2,TRANSFORM_TEX(node_8041, _Hologram_Mask2));
                float3 node_8592 = ((_Hologram_Color.rgb*_Hologram_Mask_var.rgb)*(_Hologram_Color.rgb*_Hologram_Mask2_var.rgb));
                float3 emissive = node_8592;
                float3 finalColor = emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
